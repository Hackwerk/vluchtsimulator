**Vlucht Simulator**

Het Museum voor Vluchtsimulatie in Someren stelt een oude vluchtsimulator beschikbaar zodat deze door leerlingen kan worden gemoderniseerd. Dit project is bedacht door de heer van Kalsbeek van het ICT College in Helmond.

Het project wordt uitgevoerd door het fictieve bedrijf Flightmatic, met als projectleden Shane van den Bogaard en Martijn Stommels. Dit project wordt opgeleverd als proftaak.

De doelstelling die door middel van dit project bereikt moet worden is om een oude vluchtsimulator Vista van Flightmatic, binnen 120 projecturen per projectlid, te vernieuwen met moderne hard- en software.

