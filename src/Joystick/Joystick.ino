/* 
 * Arduino USB Joystick HID for Flightsimulator
 */

#undef DEBUG
//#define DEBUG

#define NUM_BUTTONS	40
#define NUM_AXES	8	       // 8 axes, X, Y, Z, etc

#define AXIS_MAX 32767
#define AXIS_MIN -32767

#define PIN_MIXTURE 3
#define PIN_BREAK 5
#define PIN_CARBHEAT 2
#define PIN_FLAPS 6
#define PIN_RESET 4

#define DEBOUNCE_DELAY 5

// these variables store the millis() of the last time the pin was LOW 
unsigned long debounceMixture = 0;
unsigned long debounceBreak = 0;
unsigned long debounceCarbheat = 0;
unsigned long debounceReset = 0;
unsigned long debounceFlaps = 0;


typedef struct joyReport_t {
    int16_t axis[NUM_AXES];
    uint8_t button[(NUM_BUTTONS+7)/8]; // 8 buttons per byte
} joyReport_t;

joyReport_t joyReport;


void setup(void);
void loop(void);
void setButton(joyReport_t *joy, uint8_t button);
void clearButton(joyReport_t *joy, uint8_t button);
void sendJoyReport(joyReport_t *report);


void setup() 
{
    delay(2000);
    Serial.begin(115200);
    delay(200);
    
    // set pinmodes
    pinMode(PIN_MIXTURE, INPUT);
    pinMode(PIN_BREAK, INPUT);
    pinMode(PIN_CARBHEAT, INPUT);
    pinMode(PIN_FLAPS, INPUT);
    pinMode(PIN_RESET, INPUT);
    
    // enable pull-up resistors
    digitalWrite(PIN_MIXTURE, HIGH);
    digitalWrite(PIN_BREAK, HIGH);
    digitalWrite(PIN_CARBHEAT, HIGH);
    digitalWrite(PIN_FLAPS, HIGH);
    digitalWrite(PIN_RESET, HIGH);
    

    for (uint8_t ind=0; ind<8; ind++) {
		joyReport.axis[ind] = ind*1000;
    }
    for (uint8_t ind=0; ind<sizeof(joyReport.button); ind++) {
        joyReport.button[ind] = 0;
    }
}

// Send an HID report to the USB interface
void sendJoyReport(struct joyReport_t *report)
{
#ifndef DEBUG
    Serial.write((uint8_t *)report, sizeof(joyReport_t));
#else
    
	//Serial.println(report->axis[0]);
	// dump human readable output for debugging
    for (uint8_t ind=0; ind<NUM_AXES; ind++) {
		Serial.print("axis[");
		Serial.print(ind);
		Serial.print("]= ");
		Serial.print(report->axis[ind]);
		Serial.print(" ");
    }
    Serial.println();
    /*for (uint8_t ind=0; ind<NUM_BUTTONS/8; ind++) {
		Serial.print("button[");
		Serial.print(ind);
		Serial.print("]= ");
		Serial.print(report->button[ind], BIN);
		Serial.print(" ");
    }*/
    Serial.print("buttons: ");
    for (uint8_t ind=0; ind < NUM_BUTTONS; ind++) {
		Serial.print((report->button[0] & (1 << ind)), BIN);
		Serial.print(' ');
	}
    
    Serial.println();
    Serial.println();
   
    
    
#endif
}

// turn a button on
void setButton(joyReport_t *joy, uint8_t button)
{
    uint8_t index = button/8;
    uint8_t bit = button - 8*index;

    joy->button[index] |= 1 << bit;
}

// turn a button off
void clearButton(joyReport_t *joy, uint8_t button)
{
    uint8_t index = button/8;
    uint8_t bit = button - 8*index;

    joy->button[index] &= ~(1 << bit);
}

void loop() 
{
    joyReport.axis[0] = map(analogRead(A0), 0, 1023, AXIS_MIN, AXIS_MAX);
    joyReport.axis[1] = map(analogRead(A1), 0, 1023, AXIS_MIN, AXIS_MAX);
	joyReport.axis[2] = map(analogRead(A2), 0, 1023, AXIS_MIN, AXIS_MAX);
	joyReport.axis[3] = map(analogRead(A3), 0, 1023, AXIS_MIN, AXIS_MAX);
	joyReport.axis[4] = map(analogRead(A4), 0, 1023, AXIS_MIN, AXIS_MAX);
	joyReport.axis[5] = map(analogRead(A5), 0, 1023, AXIS_MIN, AXIS_MAX);
  
    if(debounceInput(PIN_MIXTURE, &debounceMixture)) setButton(&joyReport, 0); else clearButton(&joyReport, 0);
    if(debounceInput(PIN_BREAK, &debounceBreak)) setButton(&joyReport, 1); else clearButton(&joyReport, 1);
    if(debounceInput(PIN_CARBHEAT, &debounceCarbheat)) setButton(&joyReport, 2); else clearButton(&joyReport, 2);
    if(debounceInput(PIN_RESET, &debounceReset)) setButton(&joyReport, 5); else clearButton(&joyReport, 4);    
    
    if(debounceInput(PIN_FLAPS, &debounceFlaps)) 
    {
		setButton(&joyReport, 3);
		clearButton(&joyReport, 4);
	}
    else
    {
		setButton(&joyReport, 4);
		clearButton(&joyReport, 3);
    }
    
    sendJoyReport(&joyReport);
  
	#ifndef DEBUG
		delay(100);
	#else
		delay(500);
	#endif
   
}

bool debounceInput(uint8_t input, unsigned long *debounceMillis)
{
	unsigned long curMillis = millis();
	
	if(digitalRead(input) == LOW)
	{
		debounceMillis = 0;
	}
	else
	{
		// when the pin go's from high to low for the first time
		if(*debounceMillis == 0)
		{
			*debounceMillis = curMillis;
		}
		else if (*debounceMillis + DEBOUNCE_DELAY <= curMillis)
		{
			return HIGH;
		}
		
	}
	
	return LOW;
}
