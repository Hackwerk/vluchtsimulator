/**
 * Flightsimulator Arduino code.
 * Handles in and output to Microsoft's Flight Simulator X using Link2FS 
 * @see http://www.jimspage.co.nz/intro.htm
 * @author Shane van den Bogaard <shanevdb@hotmail.nl>
 * @author Martijn Stommels <martijn@martijnpc.nl>
 */ 

#include "settings.h"
#include "meters.h"
#include "link2fs.h"


void setup() {
	initMeters();	
		
	// init serial
	Serial.begin(115200);
}

void loop() {
	
	link2fsMainLoop();
	metersMainLoop();
  
	delay(5);
  
}
