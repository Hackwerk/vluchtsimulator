#include "link2fs.h"
#include "meters.h"

#include "Arduino.h"

static char getChar();
static void handleLessThan();
static void getNextChar();
static int getInt(uint8_t lenght, uint8_t decimalLenght, bool isSigned);

static char codeIn;
static char curChar;


void link2fsHandleIncomming()
{
	
}
void link2fsMainLoop()
{	
	if (Serial.available()) {  //Check if anything there
		codeIn = getChar();      //Get a serial read if there is.
		//if (CodeIn == '=') {EQUALS();} // The first identifier is "=" ,, goto void EQUALS
		if (codeIn == '<') handleLessThan();// The first identifier is "<" ,, goto void LESSTHAN
		//if (CodeIn == '?') {QUESTION();}// The first identifier is "?" ,, goto void QUESTION
		//if (CodeIn == '/') {SLASH();}// The first identifier is "/"  ,, goto void SLASH (Annunciators)
	}
}

static char getChar()// Get a character from the serial buffer(Dont touch)
{
  while(Serial.available() == 0);// wait for data (Dont touch)
  return((char)Serial.read());// (Dont touch) Thanks Doug
}

static void handleLessThan() // <
{
	getNextChar();	
	switch(curChar)
	{
		case 'R' :	
				handleAttitudeRoll(getInt(3, 2, true));
			break;
			
		case 'Q' :	
			handleAttitudePitch(getInt(3, 2, true));
			break;
		case 'P' :	
			handleAirspeed(getInt(3, 0, false));
			break;
		
		case 'D' :	
			handleAltitude(getInt(5, 0, false));
			break;
		
		case 'T' :	
			handleTacho(getInt(5, 0, false));
			break;
			
		case 'L' :	
			handleVerticalspeed(getInt(5, 0, true));
			break;
		
	}
	
}

static int getInt(uint8_t lenght, uint8_t decimalLenght, bool isSigned)
{
	String str = "";
	str.reserve(lenght + 1);
	
	if(isSigned)
	{
		getNextChar();
		if(curChar == '-') str += curChar;
	}

	for(uint8_t i = 0; i < lenght; i++)
	{
		str += getChar();
	}

	if(decimalLenght > 0)
	{
		getChar(); // skip dot

		// skip decimals
		for(uint8_t i = 0; i < decimalLenght; i++)
		{
			getChar();
		}
	}
	return str.toInt();
}

static void getNextChar()
{
	curChar = getChar();
}
