#include "motorfeedback.h"

Motorfeedback::Motorfeedback (	uint8_t pinA, 
								uint8_t pinB,
								uint8_t potPin,
								int potMin,
								int potMax,
								int	posMin,
								int posMax,
								uint8_t hysteresis)
{
	_pinA = pinA;
	_pinB = pinB;
	_potPin = potPin;
	_potMin = potMin;
	_potMax = potMax;
	_posMin = posMin;
	_posMax = posMax;
	_hysteresis = hysteresis;
	
	pinMode(pinA, OUTPUT);
	pinMode(pinB, OUTPUT);
	setPosition(0);
}

int Motorfeedback::_mapPosition(int value)
{
	return map(value, _posMin, _posMax, _potMin, _potMax);
}

void Motorfeedback::setPosition(int value)
{
	int mappedPosition = _mapPosition(value);
	//Serial.println("mapped:" + mappedPosition);
	//Serial.println("max:" + _potMax);
	//Serial.println("min:" + _potMin);
	if(mappedPosition > _potMax)
	{
		_requestedValue = _potMax;
	}
	else if (mappedPosition < _potMin)
	{
		_requestedValue = _potMin;
	}
	else
	{
		_requestedValue = mappedPosition;
	}
}

void Motorfeedback::checkPosition()
{
	int measuredValue = analogRead(_potPin);
	
	/*Serial.print("measured: ");
	Serial.println(measuredValue);
	Serial.print("_requested: ");
	Serial.println(_requestedValue);*/
	
	//delay(500);
	if(_requestedValue > measuredValue && _requestedValue + _hysteresis > measuredValue)
	{
		digitalWrite(_pinA, LOW);
		digitalWrite(_pinB, HIGH);
	}
	else if (_requestedValue < measuredValue && _requestedValue < measuredValue - _hysteresis)
	{
		digitalWrite(_pinA, HIGH);
		digitalWrite(_pinB, LOW);
	}
	else 
	{
		digitalWrite(_pinA, LOW);
		digitalWrite(_pinB, LOW);
	}
}
