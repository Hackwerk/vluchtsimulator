#include "Arduino.h"

class Motorfeedback
{
	public:
		Motorfeedback (	uint8_t pinA, 
						uint8_t pinB,
						uint8_t potPin,
						int potMin,
						int potMax,
						int	posMin,
						int posMax,
						uint8_t hysteresis
						);
						
		void setPosition(int value);
		void checkPosition();
		
		
	private:
		int _mapPosition(int value);
		
		uint8_t _pinA;
		uint8_t _pinB;
		uint8_t _potPin;
		int _requestedValue;
		int _potMin;
		int _potMax;
		int	_posMin;
		int _posMax;
		uint8_t _hysteresis;
	
};
