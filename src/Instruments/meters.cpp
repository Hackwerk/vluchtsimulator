#include "meters.h"
#include "settings.h"
#include "Arduino.h"
#include "motorfeedback.h"
#include "steppermotorfeedback.h"

static Motorfeedback attitudeRollMotorfeedback(
												ATTITUDE_ROLL_HBRIGE_PINA,
												ATTITUDE_ROLL_HBRIGE_PINB,
												ATTITUDE_ROLL_POT_PIN,
												ATTITUDE_ROLL_POT_MIN,
												ATTITUDE_ROLL_POT_MAX,
												ATTITUDE_ROLL_POS_MIN,
												ATTITUDE_ROLL_POS_MAX,
												ATTITUDE_ROLL_HYSTERESIS
												);
												
static Motorfeedback attitudePitchMotorfeedback(
												ATTITUDE_PITCH_HBRIGE_PINA,
												ATTITUDE_PITCH_HBRIGE_PINB,
												ATTITUDE_PITCH_POT_PIN,
												ATTITUDE_PITCH_POT_MIN,
												ATTITUDE_PITCH_POT_MAX,
												ATTITUDE_PITCH_POS_MIN,
												ATTITUDE_PITCH_POS_MAX,
												ATTITUDE_PITCH_HYSTERESIS
												);       
												
static StepperMotorFeedback altitudeStepperMotorFeedback(
	ALTITUDE_PINSTEP,
	ALTITUDE_PINDIR,
	ALTITUDE_STEPPER_MAX,
	true,
	ALTITUDE_POS_MAX
);     								
									

void initMeters(void)
{
	pinMode(AIRSPEED_PIN, OUTPUT);
	pinMode(TACHO_PIN, OUTPUT);
	
	pinMode(VERTICALSPEED_HBRIGE_PINA, OUTPUT);
	pinMode(VERTICALSPEED_HBRIGE_PINB, OUTPUT);
	pinMode(VERTICALSPEED_HBRIGE_ENABLE, OUTPUT);
	
	pinMode(TURNINDICATOR_HBRIGE_PINA, OUTPUT);
	pinMode(TURNINDICATOR_HBRIGE_PINB, OUTPUT);
	pinMode(TURNINDICATOR_HBRIGE_ENABLE, OUTPUT);
	
}

void metersMainLoop()
{
	attitudeRollMotorfeedback.checkPosition();
	attitudePitchMotorfeedback.checkPosition();
	altitudeStepperMotorFeedback.checkPosition();
}

void handleAirspeed(long value)
{
    if(value < AIRSPEED_MAX)
    {	
      analogWrite(AIRSPEED_PIN, map((value + AIRSPEED_CALIBRATION), AIRSPEED_MIN, AIRSPEED_MAX, 0, 255));
    }
}

void handleTacho(long value)
{
	if(value < TACHO_MAX)
	{
		analogWrite(TACHO_PIN, map((value + TACHO_CALIBRATION), TACHO_MIN, TACHO_MAX, 0, 255));
	}
}

void handleAttitudeRoll(int value)
{
	attitudeRollMotorfeedback.setPosition(value);
	
	handleTurnindicator(value);
}

void handleAttitudePitch(int value)
{
	attitudePitchMotorfeedback.setPosition(value);
}

void handleVerticalspeed(int value)
{
	if(value > VERTICALSPEED_MAX || -value > VERTICALSPEED_MAX) return;
	
	uint8_t mappedValue = 0;
	
	if(value > 0)
	{
		digitalWrite(VERTICALSPEED_HBRIGE_PINA, HIGH);
		digitalWrite(VERTICALSPEED_HBRIGE_PINB, LOW);
		
		mappedValue = map((value + VERTICALSPEED_CALIBRATION), VERTICALSPEED_MIN, VERTICALSPEED_MAX, 0, 255); 
		
	}
	else if(value < 0)
	{
		digitalWrite(VERTICALSPEED_HBRIGE_PINA, LOW);
		digitalWrite(VERTICALSPEED_HBRIGE_PINB, HIGH);
		
		// we can only use positive numbers
		mappedValue = map((-value + VERTICALSPEED_CALIBRATION), VERTICALSPEED_MIN, VERTICALSPEED_MAX, 0, 255); 
		
	}
	else
	{
		digitalWrite(VERTICALSPEED_HBRIGE_PINA, LOW);
		digitalWrite(VERTICALSPEED_HBRIGE_PINB, LOW);
		
		mappedValue = 0;
	}
	
	analogWrite(VERTICALSPEED_HBRIGE_ENABLE, mappedValue);
}

void handleAltitude(int value)
{
	altitudeStepperMotorFeedback.setPosition(value);
}

void handleTurnindicator(int value)
{
	if(value > TURNINDICATOR_MAX || -value > TURNINDICATOR_MAX) return;
	
	uint8_t mappedValue = 0;
	
	if(value > 0)
	{
		digitalWrite(TURNINDICATOR_HBRIGE_PINA, HIGH);
		digitalWrite(TURNINDICATOR_HBRIGE_PINB, LOW);
		
		mappedValue = map((value + TURNINDICATOR_CALIBRATION), TURNINDICATOR_MIN, TURNINDICATOR_MAX, 0, 255); 
		
	}
	else if(value < 0)
	{
		digitalWrite(TURNINDICATOR_HBRIGE_PINA, LOW);
		digitalWrite(TURNINDICATOR_HBRIGE_PINB, HIGH);
		
		// we can only use positive numbers
		mappedValue = map((-value + TURNINDICATOR_CALIBRATION), TURNINDICATOR_MIN, TURNINDICATOR_MAX, 0, 255); 
		
	}
	else
	{
		digitalWrite(TURNINDICATOR_HBRIGE_PINA, LOW);
		digitalWrite(TURNINDICATOR_HBRIGE_PINB, LOW);
		
		mappedValue = 0;
	}
	
	analogWrite(TURNINDICATOR_HBRIGE_ENABLE, mappedValue);
}
