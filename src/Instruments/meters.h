void initMeters(void);

void metersMainLoop();

void handleAirspeed(long value);
void handleTacho(long value);
void handleAttitudeRoll(int value);
void handleAttitudePitch(int value);
void handleAltitude(int value);
void handleVerticalspeed(int value);
void handleTurnindicator(int value);
